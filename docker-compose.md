
# Docker Compose

## Boilerplate

Thanks https://docs.docker.com/compose/gettingstarted/

``` yaml
version: "3.9"
services:
  web:
    build:
      context: ./dir
      dockerfile: Dockerfile-alternate
      args:
        buildno: 1
    ports:
      - "8000:5000"

  redis:
    image: "redis:alpine"
```

## Background service

Keep a container open if it's running a background process (e.g., systemd service).

Thanks: https://stackoverflow.com/a/55953120/17054849

Additional potential solution here may cover other cases: https://stackoverflow.com/a/45450456/17054849

``` yaml
version: "3.9"
services:
  ssh_server:
    image: openssl_experiment/ssh_server
    # Attach a TTY
    tty: true
    build:
      context: .
      dockerfile: server.Dockerfile
```
