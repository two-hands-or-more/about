
# List volumes

`sudo lvdisplay`

# Expand volume

`sudo lvextend --resizefs -L +2G /dev/mapper/vg_cloud-LogVol00`
