

# ML Ops

## MLOps: Continuous delivery and automation pipelines in machine learning

Reference: https://cloud.google.com/solutions/machine-learning/mlops-continuous-delivery-and-automation-pipelines-in-machine-learning

- Describes how DevOps should be applied to ML.
- Defines schema skews, data skews, and training-serving skews
- Defines feature stores.
- Recommends metadata management for each run of the ML pipeline

## Architecting a Machine Learning Pipeline

Reference: https://towardsdatascience.com/architecting-a-machine-learning-pipeline-a847f094d1c7

Calls out the following stages, with detailed architectural advice:
1. Ingest
    - Gather raw data
1. Prepare
    - Look for data and schema skews.
    - Put data in a feature store
1. Split
    - Split into training and test data sets.
1. Train
    - Use an algorithm to train a model
1. Evaluate
    - Evaluate predictive performance
1. Deploy
    - Stand up an execution cluster for running the model
        at scale.
1. Score
    - Use a deployed model to evaluate live data
1. Monitor
    - Check readings from deployed models and kill bad ones.

## Rules of ML

Reference: https://developers.google.com/machine-learning/guides/rules-of-ml/#training-serving_skew

Long list of best-practices for ML.

## Reddit discussion of Data Science vs Data Engineering

Reference: https://www.reddit.com/r/dataengineering/comments/jjvbox/opinions_on_data_scientists_using_airflow/

It starts off as a discussion about Airflow (a particular software for workflow execution, knowing what it is isn't important), but it very quickly turns into an in-depth discussion of how people are trying to manage the divide between data science and data engineering.
