# Adding dotfiles to a new system

``` bash
git clone --bare git@repo dotfiles
git --git-dir=$HOME/dotfiles/ --work-tree=$HOME checkout
# Fix collisions, then...
source ~/.bash_profile
dot config --local status.showUntrackedFiles no
```

# Packaging

If a system cannot reach the gitlab server to clone this repo, tar the dotfiles up and `scp` them wherever you need
them. To do this, run:

``` bash
dot archive -o dotfiles.tar.gz HEAD
```
