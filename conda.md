
## Build scripts (build.sh, bld.bat)

Reference: https://docs.conda.io/projects/conda-build/en/latest/resources/build-scripts.html

- Defines the logic that carries out the build steps.
- Can also include install steps.
- Anything that the script copies into `$PREFIX` or `%PREFIX%` will be included in the package.
    - If you're not going to be deploying to PyPI, this saves a lot of time.
- There are _many_ env vars available to the build script. See [here](https://docs.conda.io/projects/conda-build/en/latest/user-guide/environment-variables.html#env-vars).
- You can include multiple output packages.
- Build scripts are optional. You can instead use the `build/script` key in `meta.yaml`.
    - All commands must function across all platforms.

## meta.yaml

Reference: https://docs.conda.io/projects/conda-build/en/latest/resources/define-metadata.html

Example ([source](https://docs.conda.io/projects/conda-build/en/latest/resources/define-metadata.html)):
``` yaml
{% set version = "1.1.0" %}

package:
  name: imagesize
  version: {{ version }}

source:
  url: https://pypi.io/packages/source/i/imagesize/imagesize-{{ version }}.tar.gz
  sha256: f3832918bc3c66617f92e35f5d70729187676313caa60c187eb0f28b8fe5e3b5

build:
  noarch: python
  number: 0
  script: python -m pip install --no-deps --ignore-installed .

requirements:
  host:
    - python
    - pip
  run:
    - python

test:
  imports:
    - imagesize

about:
  home: https://github.com/shibukawa/imagesize_py
  license: MIT
  summary: 'Getting image size from png/jpeg/jpeg2000/gif file'
  description: |
    This module analyzes jpeg/jpeg2000/png/gif image header and
    return image size.
  dev_url: https://github.com/shibukawa/imagesize_py
  doc_url: https://pypi.python.org/pypi/imagesize
  doc_source_url: https://github.com/shibukawa/imagesize_py/blob/master/README.rst
```

## Conda package format

Refs: https://docs.conda.io/projects/conda-build/en/latest/resources/package-spec.html

A conda package is a .tar.bz2 file that contains an `info/` directory containing metadata and a collection of files
that are unpacked to a prefix location.
- Empty directories are not included.
- Name format is: `<name>-<version>-<build>.tar.bz2`

Metadata: `info/` directory
- Contains all metadata about a package
- `info/index.json`:
    - Basic package info: Nmae, version, build string, dependencies.
    - Related to repodata.json
    - Fields:
        - `name`: Lowercase name of the package
        - `version`: Package version. 
        - `build`: Build string. Differentiates packages that have identical names and packages. Helps with having
            different packages for python 3.4 vs. 3.7, bugs in the build process, different optional dependencies.
        - `build_number`: Non-negative number representing the build number of the package. Unlike `build`, this field is
            is inspected by Conda to sort packages that have otherwise identical identifiers.
        - `depends`: Lit of dependency specifications.
        - `arch`: Optional architecture specification for the package (e.g., `x86_64`)
        - `platform`: Optional indicator of te OS that the package is built for. Conda doesn't currently use this key - usually
            distinguished by the repository subdirectory
- `info/files`
    - Lists all files that are part of the package. Line delimited.
    - Any files not listed here are not linked when the package is installed.
    - Directory delimitor for the paths should always be `/`, even on Windows.
- `info/has_prefix`
    - Optional.
    - Lists all files that contain hard-coded build prefix or placholder prefix which needs
        to be replaced by the install prefix at install time.
    - By default, just the path. Can also include space-delimited placeholder, mode (text or binary), and path.
- `info/license.txt`
    - It is what it says it is.
- `info/no_link`
    - Lists all files that can't be linked (soft or hard) into environments and that should be copied instead.
- `info/about.json`
    - Information from the "About" section of the meta.yaml file.
- `info/recipe`
    - A dir containing the build recipe.
- `meta.yaml.rendered`
    - The rendered build recipe.
    - Included only when `include_recipe: True` in the `build` section of `meta.yaml`.

Link and unlink scripts
- Scripts can be added to execute tasks before and after the link and unlink steps.

### Repository structure

Conda repositories and conda channels refer to the same thing.

Repos are a dreictory tree served over HTTPS that contains platform subdirectories.
Each subdir contains conda packages and a repository index - `repodata.json`.

Use `conda index` to create an index from the packages within a directory.

Example repo structure (again, [source](https://docs.conda.io/projects/conda-build/en/latest/resources/package-spec.html)):
```
<some path>/linux-64/repodata.json
                     repodata.json.bz2
                     misc-1.0-np17py27_0.tar.bz2
           /win-32/repodata.json
                   repodata.json.bz2
                   misc-1.0-np17py27_0.tar.bz2
```

### Package match specifications

Format of a package specification contains between 1 and 3 parts:
1. Exact name of the package
2. Version - may contain special characters.
    - `|`: OR
    - `*`: Standard wildcard
    - `<`, `>`, `<=`, `>=`, `==`, `!=`: Semantic versioning relational operators.
        Follows PEP-440. Pre-release versioning is suppored (e.g., ">1.0b4 will match 1.0b5 and 1.0rc1 but not 1.0b4 or 1.0a5")
    - `,`: AND
3. Build string
    - When there are 3 parts, the second part must be exact.

