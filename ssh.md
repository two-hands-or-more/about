# Generate SSH key

For modern (mosts) systems:

``` bash
ssh-keygen -t ed25519 -C "your_email@example.com"
```

Some systems (e.g., OpenWRT) do not support `ed25519`. In this case, use:

``` bash
ssh-keygen -t rsa -b 4096 -C "your_email@example.com"
```

# Agent Forwarding - manually enable

``` bash
ssh -o ForwardAgent=yes myhost
```

# SSH Port Forwarding

``` git
ssh -NL localhost:8000:localhost:8000 remotehostname
```
