
# Using Vagrant to quickly set up R&D virtual machines

There's lots of software that I want to take out for a test drive. I generally
prefer to avoid installing this directly on my host system's operating system and
instead like to set up a virtual machine whenever practical (this avoids bloat
on the host OS, lets me more easily reset if things go terribly wrong, lets
me use different versions of dependencies for different projects, ...). Problem
is, manually creating VMs can be a bit time intensive and things can quickly
become disorganized.

Will Vagrant make it easier to spin up short-lived R&D virtual machines?

## Goal

I have a fairly beefy server (named `cavalry`) that I specifically built for hosting large
numbers (at least, by homelab standards) of virtual machines. This machine has Virtualbox
and Vagrant installed on it. I typically work by `ssh`ing directly into the server from
my more comfortable laptop.

My goal is to be able to set up a simple VM (Debian, Ubuntu, whatever) with an IP address
on my homelab network. In otherwords, the VM should not be NATted through `cavalry`, but
should be directly accessible from the wider network without doing any port forwarding
through `cavalry`.

Note: There are several warnings throughout the Vagrant site that Vagrant-based virtual
machines are somewhat insecure by design. Vagrant's designed to be efficient for developers,
not for managing production infrastructure. In particular, it has a few warnings about
configuring VMs for public access (i.e., allowing things outside of the host machine to
access the VM). I'm not concerned about these issues because I specifically set up a development
homelab network that only I have access to. So, adding VMs to the wider network is exactly
as secure as hiding them behind the host machine.

## Vagrant examples

Notes from working through the Vagrant tutorials.

[**Getting started**](https://learn.hashicorp.com/tutorials/vagrant/getting-started-index?in=vagrant/getting-started)

1. Created a sample project directory (`vagrant_example/`) and `cd` into it.
1. `vagrant init hashicorp/bionic64`
    - This [intializes](https://www.vagrantup.com/docs/cli/init) the current directory
        with a Vagrantfile
1. `vagrant up` to start the VM that's defined by the Vagrantfile created above.
1. From `cavalry`, the new VM can be accessed with `vagrant ssh`.
1. Within the VM, `ip addr` shows that its IP address is 10.0.2.15/24.
    - That IP isn't in my homelab network, so this machine shouldn't be accessible
        from outside of `cavalry`. No surprise there.
    - Correct, `ssh 10.0.2.15` from my laptop hangs.
    - Suprisingly, `ssh 10.0.2.15` from `cavalry` also hangs. I can only access with `vagrant ssh`. What's up with that?
    - When I access the VM, it states that I last logged in from `10.0.2.15/24`. `cavalry` (the machine from which I
        ran `vagrant ssh`) is 10.49.60.115/16. So, either Vagrant or Virtualbox is setting up a subnet for the new VM.
    - [This page](https://www.vagrantup.com/docs/networking/private_network) states "This will automatically assign
        an IP address from the reserved address space." This suggests that this is a Vagrant setting.
    - Anyways, this is interesting, but not relevant to my current task. I want to give the VM an IP address on my homelab
        network, and I think I just found the setting for that. I'll keep an eye on where the 10.0.2.0/24 subnet
        is coming from, but it's not relevant at this time.

## Adding the VM to my homelab network.

The default `Vagrantfile` contains this section:

```
  # Create a public network, which generally matched to bridged network.
  # Bridged networks make the machine appear as another physical device on
  # your network.
  # config.vm.network "public_network"
```

That sounds exactly like what I want. Uncommented the the `config.vm.network ...` line, then
ran `vagrant reload --provision`.

The reload asked me the following question: "Which interface should the network bridge to?" Cool! My
answer is `enp39s0`. It doesn't seem to accept that answer. Just keeps asking the same question
over and over. 

[This](https://stackoverflow.com/questions/33250304/how-to-automatically-select-bridged-network-interfaces-in-vagrant) SO
post says that the prompt should list out options. Looking a bit higher up in the output, I see the list of
interfaces. Entering the number for the desired network appears to have worked.

`ssh`ing into the VM and running `ip addr` shows a network interface in the VM with IP address `10.49.54.11/16`. That's what
I wanted!

`ssh 10.49.54.11` works from `cavalry`, but not from my laptop.

Also, it looks like the default hostname is `vagrant`.

Oh, interesting. I just changed to a different server on my homelab network (a raspberry pi that's running some
network utilities), and I was able to contact `vagrant`. I have my network set up with different subnets for
wireless and wired. It looks like anything on the wired network can contact `vagrant`, but the wifi network
cannot get through. So, looks like I need to do some debugging on my janky, custom-built raspberry pi "router". I've
been meaning to upgrade that to a "real" solution for a while, though, so I'm going to hold off on that debugging.
