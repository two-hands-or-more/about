
# Documentation on setting up a development virtual machine

## Installing the Proxmox guest agent

1. Install: `sudo apt-get install qemu-guest-agent`
1. Do a full stop and start using the ProxMox UI.
    - Rebooting or `sudo shutdown now` are not sufficient.

Refs: https://pve.proxmox.com/wiki/Qemu-guest-agent

## Setting up disks

I have two disks mounted currently:
1. `/dev/sda`
    - Underlying block device is spinning rust.
    - Serves as the root disk.
    - Configured during Ubunutu's install.
    - Uses LVM
1. `/dev/sdb`
    - Underlying block device is SSD.
    - Intention is to use this as disk space for the home
        drive.

Need to configure `/dev/sdb`:
1. Format the disk
    - `sudo fdisk /dev/sdb`
    - Create new primary partition using default settings.
1. `sudo pvcreate /dev/sdb1`
1. `sudo vgcreate user-home /dev/sdb1`
1. `sudo lvcreate -n user-home -l 100%FREE user-home`
1. `sudo mkfs.ext4 /dev/user-home/user-home`
1. Find block ID of the new LVM volume with `blkid`
1. Add the block ID to `/etc/fstab`
    - `UUID=block_id       /home/    ext4    defaults        0       2`
1. Mount all with `sudo mount -a`
    - This is hit-and-miss. I had to find where `/dev/user-home/user-home` linked
        to, then tracked that down in `/dev/disk/by-uuid/` to find the correct UUID.
1. Set up a new home directory
    - `sudo cp -R /etc/skel/ /home/thom`
    - `sudo chown -R thom.thom /home/thom/`
1. Test login with a new SSH session.
1. Reboot and check mounts.

References:
- https://www.tecmint.com/add-new-disks-using-lvm-to-linux/
- https://web.mit.edu/rhel-doc/5/RHEL-5-manual/Cluster_Logical_Volume_Manager/LV_create.html
- https://serverfault.com/a/733121

## Install Docker

First, follow instructions [here](https://docs.docker.com/engine/install/ubuntu/#install-using-the-repository)
for basic install.

Then:
1. `sudo usermod -aG docker thom`
1. `sudo apt-get install docker-compose`
1. Enable usernamespace remapping.
    - Add the following to ``;
        ```
        {
            "userns-remap": "thom"
        }
        ```
    - Test that files are showing up as owned by the correct person by
        running:
        `docker run --rm -it -v $(pwd)/tmp:/data ubuntu touch /data/hello-world`
        - If it doesn't show up correctly, make sure you've updated `/etc/subuid`
            and `/etc/subgid` with `thom:1000:1` to remap in-container root to on-host
            thom.

References: 
- https://docs.docker.com/engine/install/ubuntu/#install-using-the-repository
- https://docs.docker.com/engine/security/userns-remap/

## Configuring Docker to store its images in a new disk

1. Added a new disk in Proxmox
1. Configured with LVM, as with the home directory.
    - Named it `docker` (`/dev/docker/docker`).
    - Configured it to mount to `/var/lib/docker/` (this
        is where docker saves its stuff).
1. Restarted docker.
1. Test run with `docker run --rm -it -v $(pwd)/tmp:/data ubuntu touch /data/hello-world`
    - Check that files show up as owned by you.
