
## Get the queries that are currently running

Thanks to LinuxAcademy

``` psql
SELECT pid, datname, usename, application_name, client_addr, xact_start, wait_event_type, wait_event, state, query
FROM pg_stat_activity WHERE application_name = 'psql';
```

## Get database size

Thanks to LinuxAcademy

Entire database:
``` psql
SELECT pg_size_pretty(pg_database_size(current_database()));
```

All tables in a schema:
``` psql
SELECT table_schema, table_name, pg_size_pretty(pg_relation_size('"'||table_schema||'"."'||table_name||'"')) AS size
FROM information_schema.tables
WHERE table_schema = 'sales'
ORDER BY size DESC;
```

## Get all all columns of a particular type

Thanks LinuxAcademy

``` psql
SELECT columns.attname as name,
 data_types.typname as type,
 class.relname as table,
 tables.schemaname as schema
FROM pg_attribute columns
INNER JOIN pg_class class ON columns.attrelid = class.oid
INNER JOIN pg_tables tables on class.relname = tables.tablename
INNER JOIN pg_type data_types 
ON columns.atttypid = data_types.oid
WHERE tables.schemaname = 'sales'
AND data_types.typname = 'varchar';
```
