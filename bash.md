# Aggressive startup flags

``` bash
set -eux -o pipefail
```

`-e`: Exit if anything gives a non-zero exit status.
`-u`: Fail if an undefined variable is used.
`-x`: Print all commands to stdout as they're run.
`-o pipefail`: If any command in a pipeline fails, use that exit status as the status of the entire pipeline.

# Command line args

Parse command line args.

``` bash
# Thanks: https://stackoverflow.com/a/14203146/5046197
named=""  # Default value
POSITIONAL=()
while [[ $# -gt 0 ]]; do
  key="$1"

  case $key in
    -n|--name)
      named="$2"
      shift
      shift
      ;;
    *)    # Positional params
      POSITIONAL+=("$1") # save it in an array for later
      shift # past argument
      ;;
  esac
done
```

# Heredoc string (multiline string)

``` bash
about=$(cat << MYEOF
My multiline
string
MYEOF
)
```
