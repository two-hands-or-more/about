
# Minimal useful `setup.py` script

``` python
from setuptools import setup, find_packages


requires = [
    'Django>=2.0.7',
    'djangorestframework>=3.8.2',
    'django-filter>=1.1.0',
    'gunicorn==19.9.0',
    'Markdown>=2.6.11',
    'pandas>=0.23.1',
]


dev_requires = [
    'pytest>=3.2.5'
]


setup(
    name='exm-proj',
    version='0.0.1',
    author='Dev THoM',
    description='Example project',
    packages=find_packages(),
    install_requires=requires,
    extras_require={
        'dev': dev_requires
    },
    # entry_points={
    #     'console_scripts': [
    #         'script_name=python.path.to.module:callable'
    #     ]
    # }
)

```
