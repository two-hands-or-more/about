
# OpenSSL common commands

## Common commands

Get cert metadata:
``` bash 
openssl x509 -in test-sshd.crt -noout -text  | less
```

Public key from private key:
``` bash
# OpenSSL format
openssl rsa -in test-ca.key -pubout

# OpenSSH format
ssh-keygen -y -f test-ca.key
```

## Setting up a dead-simple CA for practice

See my notes [here](https://gitlab.com/two-hands-or-more/about/-/issues/2#note_739410443)

- Generate private key: `openssl genrsa -des3 -out hlab-ca.key 2048`
- Generate root cert: `openssl req -x509 -new -nodes -key hlab-ca.key -sha256 -days 18250 -out hlab-ca.pem`
    - Valid for 50 yrs.
- Set up a cert for pve1.hlab
    - Create key: `openssl genrsa -out pve1.hlab.key 2048`
    - Create CSR: `openssl req -new -key pve1.hlab.key -out pve1.hlab.csr`
    - Define `pve1.hlab.ext`:
    ```
    authorityKeyIdentifier=keyid,issuer
    basicConstraints=CA:FALSE
    keyUsage = digitalSignature, nonRepudiation, keyEncipherment, dataEncipherment
    subjectAltName = @alt_names

    [alt_names]
    DNS.1 = pve1.hlab
    IP.1 = 10.50.0.245
    ```
    - Gen cert: `openssl x509 -req -in pve1.hlab.csr -CA hlab-ca.pem -CAkey hlab-ca.key -CAcreateserial \hom@ca-exp:~$ openssl x509 -req -in pve1.hlab.csr -CA hlab-ca.pem -CAkey hlab-ca.key -CAcreateserial -out pve1.hlab.crt -days 825 -sha256 -extfile pve1.hlab.ext`
