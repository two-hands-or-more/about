# Notes about the homelab.

## Standard Operating Procedures

### Imaging a Raspberry Pi

This section documents the process used for writing a new image to an SD card for use in a Raspberry Pi.
This includes preparing the image for use with Ansible.

1. Write the default image with: `sudo dd bs=1M if=~/stuff/iso/raspbian/2021-10-30-raspios-bullseye-armhf-lite.img of=/dev/sdc conv=sync`
1. This creates two paritions on the SD card (`boot` and `rootfs`). Remount both of these. These instructions assume they're
    mounted to `/media/me/boot` and `/media/me/rootfs`, respectively.
1. Enable SSH access: `touch /media/me/boot/ssh`
1. Configure a hostname: `vim /media/me/rootfs/etc/hostname`
    - Add the new hostname to the Ansible inventory.
1. Add your public key to the `pi` user
    ``` bash
    mkdir /media/me/rootfs/home/pi/.ssh
    chmod 0700 /media/me/rootfs/home/pi/.ssh
    cat ~/.ssh/id_ecdsa.pub > /media/me/rootfs/home/pi/.ssh/authorized_keys
    chmod 0600 /media/me/rootfs/home/pi/.ssh/authorized_keys
    ```
1. Give `pi` passwordless root
    - `sudo visudo -f /media/me/rootfs/etc/sudoers`
    - At the bottom, add `pi    ALL=(ALL)   NOPASSWD:ALL`
1. Lock `pi`'s password
    - `sudo vim /media/me/rootfs/etc/shadow`
    - Add a `!` to `pi`'s password hash.
1. Eject the SD card plug it into the Pi.

Cool stuff to do someday to improve these instructions: Use `chroot` so we can use normal commands to edit these files; Write scripts
to automate this process.

### Writing Raspbian to an SD card on a Mac

These notes are from using a Mac.

Thanks to [this](https://www.raspberrypi.org/documentation/installation/installing-images/mac.md).

1. Download raspbian [here](https://www.raspberrypi.org/downloads/raspbian/)
1. If the image you download has an `.xz` extension, unzip it: `gunzip 2019-09-26-raspbian-buster-lite.img`
1. Validate checksum with `shasum -a 256 ~/Desktop/2019-09-26-raspbian-buster-lite.img`
1. Use `diskutil list` to find the SD card.
    - It's `/dev/disk2`.
1. Unmount that device with: `diskutil unmountDisk /dev/diskN`
1. Burn the image onto the SD card with `sudo dd bs=1m if=~/Desktop/2019-09-26-raspbian-buster-lite.img of=/dev/diskN conv=sync`
1. Eject with `sudo diskutil eject /dev/rdiskN`

When you boot the pi:
1. `ssh pi@raspberrypi`. Password is `raspberry`.
1. `sudo raspi-config`
