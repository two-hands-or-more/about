# Check for security updates

Thanks: https://serverfault.com/a/424548

``` bash
apt-get upgrade -s | grep -i security
```

# Debian install with BTRFS

BTRFS: https://www.youtube.com/watch?v=bQRWNr3ZNfc
With encryption: https://mutschler.dev/linux/ubuntu-btrfs-20-04/#step-3-optional-optimize-mount-options-for-ssd-or-nvme-drives
