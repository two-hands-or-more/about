
# Secure Software Development Practices and Expectations

My literature review is available here: https://gitlab.com/two-hands-or-more/about/-/issues/13.

Notes on EO 14028: https://gitlab.com/two-hands-or-more/about/-/issues/13#note_838652405
* Purchasers will expect vendors to produce statements that their software is secure.
* EO 14028 is a subset of zero trust networking, which I will be reviewing [here](https://gitlab.com/two-hands-or-more/about/-/issues/17).

Notes on secure software development practices: https://gitlab.com/two-hands-or-more/about/-/issues/13#note_843406163
* Describes expectations for:
    * Preparing the organization
    * Protecting the software
    * Producing well-secured software
    * Responding to vulnerabilities
